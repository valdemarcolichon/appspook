package spook.riva.com.appspook;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import spook.riva.com.appspook.ui.main.NavigationViewRelatos;

public class Splash extends AppCompatActivity {

    @BindView(R.id.splash_text_spook)
    TextView mSplash_text_spook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
              /*  FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user != null) {
                    startActivity(new Intent(Splash.this, NavigationViewRelatos.class));
                    finish();
                }else{
                    startActivity(new Intent(Splash.this, AccessRelato.class));
                    finish();
                }*/
                startActivity(new Intent(Splash.this, NavigationViewRelatos.class));
                finish();
            }
        }, 500);
    }
}
