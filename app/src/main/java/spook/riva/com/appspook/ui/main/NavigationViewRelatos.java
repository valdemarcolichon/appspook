package spook.riva.com.appspook.ui.main;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import spook.riva.com.appspook.R;
import spook.riva.com.appspook.ui.accesos.AccessRelato;
import spook.riva.com.appspook.ui.fragmentos.AplicacionesExtras;
import spook.riva.com.appspook.ui.fragmentos.Creaciones;
import spook.riva.com.appspook.ui.fragmentos.Favorite;
import spook.riva.com.appspook.ui.fragmentos.Help;
import spook.riva.com.appspook.ui.fragmentos.Inicio;
import spook.riva.com.appspook.ui.postear.SendHistory;
import spook.riva.com.appspook.utilidades.BackgroundSoundService;

public class NavigationViewRelatos extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView mNavigationView;
    private Dialog MyDialog;
    private DatabaseReference mDatabaseVersionApp;
    private DatabaseReference mMensajeShare;
    private InterstitialAd mInterstitialAd;
    private ImageView mMenu_profile_image;
    private TextView mMenu_profile_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_view_relatos);

        setToolbar("Spook");
        MyDialog = new Dialog(NavigationViewRelatos.this);
        menu_inicio();


        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2031757066481790/6592034792");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        MyDialog = new Dialog(NavigationViewRelatos.this);

        validarActualizacion();

        Intent svc = new Intent(getApplicationContext(), BackgroundSoundService.class);
        startService(svc);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        //getMenuInflater().inflate(R.menu.navigation_view_relatos, menu);

        mMenu_profile_image = (ImageView) findViewById(R.id.menu_profile_image);
        mMenu_profile_name = (TextView) findViewById(R.id.menu_profile_name);
        //mMenu_profile_email = (TextView) findViewById(R.id.menu_profile_email);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if(user != null) {
            Uri photoUrl = user.getPhotoUrl();
            String name = user.getDisplayName();
            String email = user.getEmail();
            String userId = user.getUid();


            if(name != null){
                mMenu_profile_name.setText(email);
            }else{
                mMenu_profile_name.setText(email);
            }
            // mMenu_profile_email.setText("Puntos: 0");

           /* Glide.with(NavigationViewRelatos.this)
                    .load(photoUrl)
                    .thumbnail(Glide.with(NavigationViewRelatos.this).load(R.drawable.item_placeholder))
                    .into(mMenu_profile_image);*/
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.nav_inicio) {
            showSnackBar("Ya estas aquí");
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_inicio) {
            menu_inicio();
            showAnuncio();

        }else if (id == R.id.nav_help) {
            menu_help();
            setToolbar("¿Quiénes Somos?");
            showAnuncio();

        }else if (id == R.id.nav_sendHistory) {
            if(user != null) {
                sendHistory();
                showAnuncio();
            }else{
                popUsuarioSinSesion();
            }
        }else if (id == R.id.nav_creaciones) {
            if(user != null) {
                menu_creaciones();
                showAnuncio();
                setToolbar("Mis Creaciones");
            }else{
                popUsuarioSinSesion();
            }
        }else if (id == R.id.nav_favorite) {
            if(user != null) {
                misFavoritos();
                showAnuncio();

                setToolbar("Mis Favoritos");
            }else{
                popUsuarioSinSesion();
            }
        }else if (id == R.id.nav_share) {
            share();
        }else if (id == R.id.nav_cerrar_sesion) {
            if(user != null) {
                cerrarSesion();
            }else{
                popUsuarioSinSesion();
            }
        }else{
            menu_inicio();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private void setToolbar(String title) {
        // Añadir la Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // openCoins();
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void menu_inicio() {


        Inicio inicio = new Inicio();
        getSupportFragmentManager().beginTransaction().replace(R.id.contenido_dinamico, inicio)
                .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
                .addToBackStack(null).commit();
    }

    private void menu_help() {
        Help help = new Help();
        getSupportFragmentManager().beginTransaction().replace(R.id.contenido_dinamico, help)
                .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
                .addToBackStack(null).commit();
    }

    private void sendHistory(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if(user != null) {
            Intent i = new Intent(NavigationViewRelatos.this, SendHistory.class);
            startActivity(i);
        }else{
           // needAccess();
            Intent i = new Intent(NavigationViewRelatos.this, SendHistory.class);
            startActivity(i);
        }
    }

    private void menu_creaciones() {
        Creaciones creaciones = new Creaciones();
        getSupportFragmentManager().beginTransaction().replace(R.id.contenido_dinamico, creaciones)
                .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
                .addToBackStack(null).commit();
    }

    private void misFavoritos() {
        Favorite favorite = new Favorite();
        getSupportFragmentManager().beginTransaction().replace(R.id.contenido_dinamico, favorite)
                .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
                .addToBackStack(null).commit();
    }

    private void misRecomendaciones() {
        AplicacionesExtras aplicacionesExtras = new AplicacionesExtras();
        getSupportFragmentManager().beginTransaction().replace(R.id.contenido_dinamico, aplicacionesExtras)
                .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
                .addToBackStack(null).commit();
    }

    public void validarActualizacion(){

        mDatabaseVersionApp = FirebaseDatabase.getInstance().getReference().child("VersionApp");
        mDatabaseVersionApp.keepSynced(true);
        mDatabaseVersionApp.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String appVersion = (String) dataSnapshot.child("version").getValue();
                String title = (String) dataSnapshot.child("title").getValue();
                String body = (String) dataSnapshot.child("body").getValue();

                Log.v("PackageInfo",""+appVersion);

                PackageInfo pInfo = null;

                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                String version = pInfo.versionName;
                int verCode = pInfo.versionCode;

                Log.v("PackageInfo",""+version);
                Log.v("PackageInfo",""+verCode);

                int verCodeActual = Integer.parseInt(appVersion);

                if(verCode < verCodeActual){
                    ModalCheckUpdate(title,body);
                    Log.v("PackageInfo",""+verCode+""+verCodeActual);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    public void showSnackBar(String msg) {
        Snackbar
                .make(findViewById(R.id.drawer_layout), msg, Snackbar.LENGTH_SHORT)
                .show();
    }

    public void showAnuncio() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            Log.v("Anuncio","click");
        }
    }
    //modal validar usuario
    public void popUsuarioSinSesion(){

        MyDialog = new Dialog(NavigationViewRelatos.this);
        MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyDialog.setContentView(R.layout.moda_need_permiso);
        MyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnModalAcessoRelato = MyDialog.findViewById(R.id.modal_need_inicia_sesion);
        Button btnModalCancel = MyDialog.findViewById(R.id.modal_need_cancel);
        TextView mModal_need_try_feature_text_body = MyDialog.findViewById(R.id.modal_need_text_body);
       // mModal_need_try_feature_text_body.setText("Pronto Estará Disponible");

        btnModalAcessoRelato.setEnabled(true);

        btnModalAcessoRelato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniciaSesion();
                MyDialog.dismiss();
            }
        });

        btnModalCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialog.dismiss();
            }
        });

        MyDialog.show();

    }

    public void ModalCheckUpdate(String title,String body){

        MyDialog = new Dialog(NavigationViewRelatos.this);
        MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyDialog.setContentView(R.layout.activity_check_update_version_app);
        MyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView mModal_check_update_title = MyDialog.findViewById(R.id.modal_check_update_title);
        TextView mModal_check_update_body = MyDialog.findViewById(R.id.modal_check_update_body);

        mModal_check_update_title.setText(title);
        mModal_check_update_body.setText(body);

        Button btnModalActualizar = MyDialog.findViewById(R.id.modal_check_update_actualizar);
        Button btnModalCancel = MyDialog.findViewById(R.id.modal_check_update_later);

        btnModalActualizar.setEnabled(true);

        btnModalActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialog.dismiss();
                Intent intent1 = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id="
                                + NavigationViewRelatos.this.getPackageName()));
                startActivity(intent1);
            }
        });

        btnModalCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialog.hide();
            }
        });

        MyDialog.show();

    }

    private void cerrarSesion() {
        FirebaseAuth.getInstance().signOut();
        iniciaSesion();
    }

    private void iniciaSesion(){
        startActivity(new Intent(NavigationViewRelatos.this, AccessRelato.class));
    }

    public void share() {

        mMensajeShare = (DatabaseReference) FirebaseDatabase.getInstance().getReference();
        mMensajeShare.keepSynced(true);

        mMensajeShare.child("Message").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String texto = (String) dataSnapshot.child("text").getValue();
                String link = (String) dataSnapshot.child("link").getValue();

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                 sendIntent.putExtra(Intent.EXTRA_TEXT,texto+" "+link);
                //sendIntent.putExtra(Intent.EXTRA_TEXT,"Esta app te hará sufrir un infarto con sus Sangrientas Lecturas, Descargala YA!! https://play.google.com/store/apps/details?id=relato.app.dems.com.relato.beta");

                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent svc = new Intent(getApplicationContext(), BackgroundSoundService.class);
        startService(svc);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent svc = new Intent(getApplicationContext(), BackgroundSoundService.class);
        stopService(svc);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Intent svc = new Intent(getApplicationContext(), BackgroundSoundService.class);
        stopService(svc);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent svc = new Intent(getApplicationContext(), BackgroundSoundService.class);
        stopService(svc);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            Log.v("Anuncio","click");
        }

    }
}
