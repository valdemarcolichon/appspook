package spook.riva.com.appspook.ui.detalle;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import spook.riva.com.appspook.R;
import spook.riva.com.appspook.ui.accesos.AccessRelato;
import spook.riva.com.appspook.ui.main.NavigationViewRelatos;

public class DetailsRelato extends AppCompatActivity implements
        TextToSpeech.OnInitListener{

    private String mPost_key = null;
    private DatabaseReference mDatabase,mMensajeShare;

    private ProgressDialog mProgresDialog;
    private TextToSpeech tts;
    private SharedPreferences prefs = null;
    private InterstitialAd mInterstitialAd;
    private Dialog MyDialog;
    private AdView mAdView;
    @BindView(R.id.postTitleDetails)
    TextView mPostTitleDetails;
    @BindView(R.id.postDescDetails) TextView mPostDescDetails;
    @BindView(R.id.image_paralax)
    ImageView mImage_paralax;

    @BindView(R.id.postRemoveDetails)
    Button mPostRemoveDetails;
    //Favorite
    android.support.design.widget.FloatingActionButton mFav_favorite;

    //@BindView(R.id.fav_favorite)

    private FirebaseAuth mAuth;
    //favorite variables
    private boolean mProcessLike;
    private DatabaseReference mDatabaseLike,mDatabasePortada;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_relato);
        ButterKnife.bind(this);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        init();
        MyDialog = new Dialog(DetailsRelato.this);
        mDatabaseLike = FirebaseDatabase.getInstance().getReference().child("Likes");
        mDatabasePortada = FirebaseDatabase.getInstance().getReference().child("Portada");
        mDatabaseLike.keepSynced(true);
        mDatabasePortada.keepSynced(true);

        mPost_key = getIntent().getExtras().getString("blog_id");
        mAuth = FirebaseAuth.getInstance();

        mFav_favorite = (android.support.design.widget.FloatingActionButton) findViewById(R.id.fav_favorite);
        mFav_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProcessLike = true;
                Log.v("TAG_LIKE","LINE click");

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


                if(user != null){
                    mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final String post_title = (String) dataSnapshot.child("title").getValue();
                            final String post_image = (String) dataSnapshot.child("image").getValue();
                            final String post_category = (String) dataSnapshot.child("category").getValue();
                            final String post_author = (String) dataSnapshot.child("author").getValue();
                            final String post_desc = (String) dataSnapshot.child("desc").getValue();
                            // Toast.makeText(BlogSingleActicity.this,""+post_title+post_desc+post_image,Toast.LENGTH_SHORT).show();

                            mDatabaseLike.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (mProcessLike){

                                        if(dataSnapshot.child(mAuth.getCurrentUser().getUid()).hasChild(mPost_key)){
                                            Log.v("TAG_LIKE","LINE NO");
                                            //  mDatabaseLike.child(mPost_key).child(mAuth.getCurrentUser().getUid()).removeValue();
                                            mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(mPost_key).removeValue();
                                            showSnackBar("Eliminado de favoritos");


                                            mFav_favorite.setImageResource(R.mipmap.ic_star_half);
                                            mProcessLike = false;
                                        }else{
                                            //mDatabaseLike.child(mPost_key).child(mAuth.getCurrentUser().getUid()).setValue("ramdom like");
                                            mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(mPost_key).child("title").setValue(post_title);
                                            mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(mPost_key).child("image").setValue(post_image);
                                            mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(mPost_key).child("author").setValue(post_author);
                                            mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(mPost_key).child("category").setValue(post_category);

/*
                                            mDatabasePortada.child("title").setValue(post_title);
                                            mDatabasePortada.child("images").setValue(post_image);
                                            mDatabasePortada.child("desc").setValue(post_desc);

*/


                                            Log.v("TAG_LIKE","LINE ramdom");
                                            mFav_favorite.setImageResource(R.mipmap.ic_star);
                                            mProcessLike = false;
                                            showSnackBar("Agregado a favoritos");
                                        }



                                        if(dataSnapshot.child(mPost_key).hasChild(mAuth.getCurrentUser().getUid())){
                                            showSnackBar("Dislike");
                                        }



                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.v("TAG_LIKE","LINE onCancelled");

                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }else{
                    showSnackBar("Necesitas Iniciar Sesión");
                    popUsuarioSinSesion();
                }


            }
        });

        mDatabaseLike.addValueEventListener(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user != null){
                    //primero la historia el id
                    if(mAuth.getCurrentUser().getUid() != null){
                        if(dataSnapshot.child(mAuth.getCurrentUser().getUid()).hasChild(mPost_key)){
                            mFav_favorite.setImageResource(R.mipmap.ic_star);
                            Log.v("TAG_LIKE","Favorito");

                        }else{
                            mFav_favorite.setImageResource(R.mipmap.ic_star_half);
                            Log.v("TAG_LIKE","no Favorito");

                        }
                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void favoriteInit() {


    }

    private void init() {

        MobileAds.initialize(this,"ca-app-pub-2031757066481790/6592034792");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2031757066481790/6592034792");

        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        if (mInterstitialAd.isLoaded()) {
            // mInterstitialAd.show();
            Log.v("Anuncio","click");
        }

       // mAdView = (AdView) findViewById(R.id.adView);
       // AdRequest adRequest = new AdRequest.Builder().build();
       // mAdView.loadAd(adRequest);

        NestedScrollView nsv = (NestedScrollView) findViewById(R.id.scroll);

        tts = new TextToSpeech(this, this);

        prefs = getSharedPreferences("relato.app.dems.com.relato.beta", MODE_PRIVATE);

        mProgresDialog= new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Historias");
        mPost_key = getIntent().getExtras().getString("blog_id");


        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String post_title = (String) dataSnapshot.child("title").getValue();
                String post_desc = (String) dataSnapshot.child("desc").getValue();
                String post_image = (String) dataSnapshot.child("image").getValue();
                String post_IdMiLectura = (String) dataSnapshot.child("IdMiLectura").getValue();
                // Toast.makeText(BlogSingleActicity.this,""+post_title+post_desc+post_image,Toast.LENGTH_SHORT).show();

                String textoCentradoDesc = post_desc;

                String text_string_center = "<html><body style='text-align:justify;'>"+textoCentradoDesc+"<body><html>";

                //Log.v("asdasvtvrt",text_string_center);
                /*****************************************/

                String justifyTag = "<html><body style='text-align:justify;background:black !important;color:#c1c0c0;font-size:15px;'>%s</body></html>";

                String dataString = String.format(Locale.US, justifyTag, text_string_center);
                WebView webViewDetail = (WebView) findViewById(R.id.webViewDetail);
                webViewDetail.loadDataWithBaseURL("", dataString, "text/html", "UTF-8", "");

                /*****************************************/

                mPostDescDetails.setText(Html.fromHtml(text_string_center), TextView.BufferType.SPANNABLE);

                mPostTitleDetails.setText(post_title);
                //mPostDescDetails.setText(post_desc);

                //validarVisibilidadAudio();
                Glide.with(getApplicationContext())
                        .load(post_image)
                        .into(mImage_paralax);

                Log.v("post_all",""+post_image+post_title+post_desc);
                showToolbar(post_title,true);

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user != null){
                    String userId = user.getUid();

                    Log.v("okey","Logueado");
                    Log.v("okey",""+mAuth.getCurrentUser().getUid());
                    Log.v("okey",""+post_IdMiLectura);
                    Log.v("okey",""+userId);
                    if(mAuth.getCurrentUser().getUid().equals(post_IdMiLectura)){
                        Log.v("okey","asd"+mAuth.getCurrentUser().getUid());
                        mPostRemoveDetails.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mPostRemoveDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgresDialog.setMessage("Removiendo Historia");
                mProgresDialog.show();
                mDatabase.child(mPost_key).removeValue();
                startActivity(new Intent(getApplicationContext(),NavigationViewRelatos.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                mProgresDialog.dismiss();
                finish();

            }
        });
    }

    @Override
    protected void onStart() {
        if (prefs.getBoolean("firstrun", true)) {
            //Intent svc = new Intent(getApplicationContext(), BackgroundSoundService.class);
            //startService(svc);

        }else{
            //Intent svc = new Intent(getApplicationContext(), BackgroundSoundService.class);
            //stopService(svc);
        }
        if (mInterstitialAd.isLoaded()) {
            //mInterstitialAd.show();
            Log.v("Anuncio","click");
        }
        super.onStart();
    }

    @Override
    public void onDestroy(){
        // Don't forget to shutdown!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        if (mInterstitialAd.isLoaded()) {
            // mInterstitialAd.show();
            Log.v("Anuncio","click");
        }
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        tts.playSilence(100, TextToSpeech.QUEUE_FLUSH,null);
        if (mInterstitialAd.isLoaded()) {
            // mInterstitialAd.show();
            Log.v("Anuncio","click");
        }
        super.onStop();
    }

    @Override
    public void onInit(int status) {
        // TODO Auto-generated method stub

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(new Locale("es","US"));

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language is not supported");
            } else {

                Log.e("TTS", "Language is  supported");
                mPostDescDetails = (TextView) findViewById(R.id.postDescDetails);
                String text = mPostDescDetails.getText().toString();

                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                tts.playSilence(100, TextToSpeech.QUEUE_FLUSH,null);

            }

        } else {
            Log.e("TTS", "Initilization Failed");
        }
    }

    private void speakOut(){

        mPostDescDetails = (TextView) findViewById(R.id.postDescDetails);
        String text = mPostDescDetails.getText().toString();

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        Log.e("TTS", ""+text);
        //  stopService(new Intent(this, BackgroundSoundService.class));

    }

    @Override
    protected void onResume() {
        //android.support.design.widget.FloatingActionButton mFabPlay =(android.support.design.widget.FloatingActionButton)  findViewById(R.id.fabPlay);
        // android.support.design.widget.FloatingActionButton mFabPause =(android.support.design.widget.FloatingActionButton)  findViewById(R.id.fabPause);
        //mFabPlay.setVisibility(View.VISIBLE);
//        mFabPause.setVisibility(View.GONE);
        if (mInterstitialAd.isLoaded()) {
            // mInterstitialAd.show();
            Log.v("Anuncio","click");
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mInterstitialAd.isLoaded()) {
            // mInterstitialAd.show();
            Log.v("Anuncio","click");
        }
        super.onPause();
    }

    //modal validar usuario
    public void popUsuarioSinSesion(){

        MyDialog = new Dialog(DetailsRelato.this);
        MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyDialog.setContentView(R.layout.moda_need_permiso);
        MyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnModalAcessoRelato = MyDialog.findViewById(R.id.modal_need_inicia_sesion);
        Button btnModalCancel = MyDialog.findViewById(R.id.modal_need_cancel);
        TextView mModal_need_try_feature_text_body = MyDialog.findViewById(R.id.modal_need_text_body);
        // mModal_need_try_feature_text_body.setText("Pronto Estará Disponible");

        btnModalAcessoRelato.setEnabled(true);

        btnModalAcessoRelato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniciaSesion();
                MyDialog.dismiss();

            }
        });

        btnModalCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialog.dismiss();
            }
        });

        MyDialog.show();

    }



    private void showSnackBar(String msg) {
        Snackbar
                .make(findViewById(R.id.coordinator), msg, Snackbar.LENGTH_LONG)
                .show();
    }


    public void share(View v) {

        mMensajeShare = (DatabaseReference) FirebaseDatabase.getInstance().getReference();
        mMensajeShare.keepSynced(true);

        mMensajeShare.child("Message").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String texto = (String) dataSnapshot.child("text").getValue();
                String link = (String) dataSnapshot.child("link").getValue();

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                // sendIntent.putExtra(Intent.EXTRA_TEXT,texto+" "+link);
                sendIntent.putExtra(Intent.EXTRA_TEXT,"Esta app te hará sufrir un infarto con sus Sangrientas Lecturas, Descargala YA!! https://play.google.com/store/apps/details?id=relato.app.dems.com.relato.beta");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void scoreBlogDetails(View v) {
        Intent intent1 = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id="
                        + getApplicationContext().getPackageName()));
        startActivity(intent1);
    }

    public void letraMas(View v) {
        mPostTitleDetails = (TextView) findViewById(R.id.postTitleDetails);
        mPostDescDetails = (TextView) findViewById(R.id.postDescDetails);

        int valorLetraDes = (int) mPostDescDetails.getTextSize();

        if(valorLetraDes < 38){
            int aumentar = valorLetraDes +10;
            Log.v("tts","tt"+valorLetraDes);
            mPostTitleDetails.setTextSize(aumentar);
            mPostDescDetails.setTextSize(TypedValue.COMPLEX_UNIT_SP,mPostDescDetails.getTextSize()+1);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            //this.moveTaskToBack(true);
          /*  Intent i = new Intent(BlogSingleActicity.this,FeedRelatos.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            startActivity(i);*/
            finish();

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void showToolbar(String tittle, boolean upButton) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(tittle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    private void iniciaSesion(){
        startActivity(new Intent(DetailsRelato.this, AccessRelato.class));
    }








}
