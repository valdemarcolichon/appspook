package spook.riva.com.appspook.ui.accesos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import spook.riva.com.appspook.R;
import spook.riva.com.appspook.ui.ValidarEmail;
import spook.riva.com.appspook.ui.main.NavigationViewRelatos;

public class AccessRelato extends AppCompatActivity   {

    //progreso
    private ProgressDialog mProgress;

    //Editext
    private EditText mAccesso_login_email;

    //@BindView(R.id.accesso_login_password)
    private EditText mAccesso_login_password;

    //@BindView(R.id.accesso_login_btn)
    private Button mAccesso_login_btn;

   // @BindView(R.id.acceso_create_register)
    private TextView mAcceso_create_register;

    //@BindView(R.id.acceso_forget_password)
    private TextView mAcceso_forget_password;

    //login
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_relato);
        init();
    }

    private void init() {

        mProgress = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();

        mAccesso_login_btn = findViewById(R.id.accesso_login_btn);

        mAcceso_create_register = findViewById(R.id.acceso_create_register);
        mAcceso_forget_password = findViewById(R.id.acceso_forget_password);


        mAccesso_login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLogin();
            }
        });

        mAcceso_create_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccessRelato.this,RegisterRelato.class));
            }
        });

        mAcceso_forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccessRelato.this,ForgetPasswordRelato.class));
            }
        });
    }

    private void startLogin() {
        Log.v("accesoPermitido", "Ingresando");


        mAccesso_login_email = findViewById(R.id.accesso_login_email);
        mAccesso_login_password = findViewById(R.id.accesso_login_password);

        String display_email = mAccesso_login_email.getText().toString();
        String display_password = mAccesso_login_password.getText().toString();

        if (!validarEmail(display_email)){
            showSnackBar("Email no válido.");
            mAccesso_login_email.setError("Email no válido.");
        }else if(display_password.length() < 6){
            showSnackBar("La contraseña es muy corta.");
            mAccesso_login_password.setError("Mínimo 6 dígitos.");
        }else{
            //showSnackBar("Accediendo");
            mProgress.setTitle("Iniciando Sessión...");
            mProgress.setMessage("Por espere mientras verificamos sus credenciales.");
            mProgress.setCancelable(true);
            mProgress.show();
            loginUser(display_email,display_password);
        }
    }

    private boolean validarEmail(String display_email) {
        ValidarEmail check = new ValidarEmail(getApplicationContext());
        boolean booleamCheckEmail = check.checkEmail(display_email);
        Log.v("checkEmail",""+booleamCheckEmail);
        return booleamCheckEmail;
    }

    private void loginUser(String display_email, String display_password) {
        mProgress.setMessage("Accediendo ...");
        mProgress.show();

        mAuth.signInWithEmailAndPassword(display_email,display_password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    accesoPermitido();
                }else{
                    Log.v("task",""+task);
                    mProgress.hide();
                    showSnackBar("Acceso denegado, Intente nuevamente.");
                    mProgress.dismiss();
                }
            }
        });

    }

    public void accesoPermitido() {
        mProgress.dismiss();
        Intent i = new Intent(AccessRelato.this, NavigationViewRelatos.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        //finish();
        Log.v("accesoPermitido", "Ingresando");
        showSnackBar("Acceso Permitido");
    }

    public void showSnackBar(String msg) {
        Snackbar
                .make(findViewById(R.id.linearAccesso), msg, Snackbar.LENGTH_SHORT)
                .show();
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }


}
